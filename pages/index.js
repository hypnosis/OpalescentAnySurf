import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Opalescent</title>
        <meta name="description" content="Opalescent: Interactive 3D NFT worlds for users, artists, musicians, developers, and entrepreneurs." />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          <code className={styles.code}>Opalescent</code>
        </h1>
        <blockquote className={styles.description}>
          Interactive 3D NFT worlds
        </blockquote>

        <div className={styles.grid}>
          <a href="#" className={styles.card}>
            <div><em>Users&nbsp;&rarr;</em></div>
            <p>Enjoy Your Time at <code className={styles.code}>Opalescent</code>!</p>
          </a>

          <a href="#" className={styles.card}>
            <div><em>Artists&nbsp;&amp;&nbsp;Musicians&nbsp;&rarr;</em></div>
            <p>Work and Publish with <code className={styles.code}>Opalescent</code>!</p>
          </a>

          <a
            href="#"
            className={styles.card}
          >
            <div><em>Developers&nbsp;&rarr;</em></div>
            <p>Discover and deploy boilerplate example <code className={styles.code}>Opalescent</code> projects.</p>
          </a>

          <a
            href="#"
            className={styles.card}
          >
            <div><em>Entrepreneurs&nbsp;&rarr;</em></div>
            <p>
              Instantly deploy your next business interlinked with or based on <code className={styles.code}>Opalescent</code> cloud-hosted or on-premises!
            </p>
          </a>
        </div>
      </main>

      <footer className={styles.footer}>
        <p className={styles.copyright}>
        <span>Copyright © 2021 Opalescent3D & all people involved. All Rights Reserved.</span>
        </p>

        <p className={styles.poweredby}>
          <span>
            <a 
              href="https://gitlab.com/hypnosis/OpalescentAnySurf"
              target="_blank"
              rel="noopener noreferrer"
            >Source code</a>
            &nbsp;is hosted by&nbsp;
            <a 
              href="https://gitlab.com/"
              target="_blank"
              rel="noopener noreferrer"
            >Gitlab</a>.
          </span>

          &nbsp;

          <span>
            <a
              href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
              target="_blank"
              rel="noopener noreferrer"
            >Powered by Vercel</a>.
          </span>
        </p>
      </footer>
    </div>
  )
}
